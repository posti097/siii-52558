//utilizar $ strace ./<nombre_programa>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#include <cstring>
 

#define MAX 100


int main() {

	
	int fd=0;

        
	/* crea el FIFO */
	if (mkfifo("./temp/FIFO_logger", 0777)<0) {
		perror("No puede crearse el FIFO");
		return(1);
	}
	/* Abre el FIFO */
	if ((fd=open("./temp/FIFO_logger", O_RDONLY ))<0) {  //lo abres como un fichero normal 
		perror("No puede abrirse el FIFO");
		return(1);
	}

	char cadena[MAX]="CADENA";
	char fin[MAX]="FIN";
	while (1){
	//while (cadena[0]!=fin[0]) {

		

		if(read(fd, cadena, sizeof(cadena))<0){
			perror("No puede leerse el FIFO");
			return(1);
		}	

		
		printf("%s \n", cadena);
	}
	close(fd);
	if(unlink("./temp/FIFO_logger")<1)
		(perror("fallo al cerrar fifo loggger"));

	return(0);
}
